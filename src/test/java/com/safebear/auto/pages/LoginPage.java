package com.safebear.auto.pages;


import com.safebear.auto.models.User;
import com.safebear.auto.pages.locators.LoginPageLocators;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebDriver;

@RequiredArgsConstructor
public class LoginPage {
        LoginPageLocators locators = new LoginPageLocators();
        @NonNull
        WebDriver driver;
        String expectedPageTitle = "Login Page";

    public String getPageTitle(){
        return driver.getTitle();
        }

    public String getExpectedPageTitle() {
        return expectedPageTitle;
    }

    public void enterUsername(String username){
        driver.findElement(locators.getUsernameFieldLocator()).sendKeys(username);
        }

        public void enterPassword(String password){
        driver.findElement(locators.getPasswordFieldLocator()).sendKeys(password);
        }

        public void clickLoginButton(){
        driver.findElement(locators.getSubmitButtonLocator()).click();
        }

        public void pressEnterToSubmitForm(){
        driver.findElement(locators.getPasswordFieldLocator()).submit();
        }

    public String getErrorMessage(){
        return driver.findElement(locators.getWarningMessageLocator()).getText();
    }


    public void login(String username, String password){
        enterUsername(username);
        enterPassword(password);
        clickLoginButton();
            }

    public void login(User userId){
        enterUsername(userId.getUserName());
        enterPassword(userId.getPassword());
        clickLoginButton();
            }
}
