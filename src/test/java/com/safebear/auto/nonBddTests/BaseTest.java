package com.safebear.auto.nonBddTests;

import com.safebear.auto.pages.LoginPage;
import com.safebear.auto.pages.ToolsPage;
import com.safebear.auto.services.UserProvider;
import com.safebear.auto.utils.Properties;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.*;

public abstract class BaseTest {

    WebDriver driver;
    LoginPage loginPage;
    ToolsPage toolsPage;
    UserProvider userProvider = new UserProvider();

    @BeforeClass
    public void setUp() {
        driver = Properties.getDriver();
        toolsPage = new ToolsPage(driver);
        loginPage = new LoginPage(driver);
    }

    @AfterClass
    public void tearDown() {
        try {
            Thread.sleep(Integer.parseInt(System.getProperty("sleep", "500")));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        driver.quit();
    }

    @AfterMethod
    public void takeScreenshotOnFailure(ITestResult testResult) {
        if (testResult.getStatus() == ITestResult.FAILURE) {
            try {
                Properties.captureScreenShot(driver, Properties.generateScreenshotFileName(testResult.getName()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
