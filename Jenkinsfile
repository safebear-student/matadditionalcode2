pipeline {
    agent any

    parameters {
        string(name: 'tests', defaultValue: 'RunCukes', description: 'cucumber tests')
        string(name: 'url', defaultValue: 'http://toolslist.safebear.co.uk:8080', description: 'live')
        string(name: 'browser', defaultValue: 'headless', description: 'default browser is headless')
        string(name: 'sleep', defaultValue: '0', description: 'set sleep to zero')
    }

    triggers {
        pollSCM('* * * * *')
    }

    stages {

        stage('Smoke tests') {
            steps {
                bat "mvn -Dtest=RunCukesSmoke test -Durl=${params.url} -Dbrowser=${params.browser} -Dsleep=${params.sleep}"
            }

        }

        stage('API Testing') {
            steps {
                bat "npm install"
                bat "npm test"
            }
        }

        stage('BDD test phase') {
            steps {
                bat "mvn -Dtest=${tests} test -Dsleep=${sleep} -Durl=${url} -Dbrowser=${browser}"
            }

            post {
                always {
                    publishHTML([
                            allowMissing         : false,
                            alwaysLinkToLastBuild: false,
                            keepAll              : false,
                            reportDir            : 'target/cucumber-reports',
                            reportFiles          : 'report.html',
                            reportName           : 'BDD report',
                            reportTitles         : ''
                    ])
                }
            }
        }

        stage('Login tests - nonBDD') {
            steps {
                bat "mvn -Dtest=LoginTests test -Dsleep=${sleep} -Durl=${url} -Dbrowser=${browser}"
            }
        }


    }
}